package com.example.encuesta.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.encuesta.dto.EncuestaRequest;
import com.example.encuesta.dto.EncuestaResponse;
import com.example.encuesta.dto.Pregunta;
import com.example.encuesta.dto.PreguntaResponseDto;
import com.example.encuesta.dto.Respuesta;
import com.example.encuesta.dto.Usuario;
import com.example.encuesta.service.impl.EncuestaService;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import org.springframework.http.ResponseEntity;
import static org.assertj.core.api.Assertions.assertThat;

class EncuestaControllerTest {
	
	@InjectMocks
	EncuestaController   encuestaController;
	
	@Mock
	EncuestaService encuestaService;

    @BeforeEach
    void setUp() {
    	MockitoAnnotations.initMocks(this);
    }

    @Test
    void encuestasConsul() {
    	List<Pregunta> liPreguntas = new ArrayList<Pregunta>();
    	Pregunta pregunta1 = new Pregunta();
    	pregunta1.setId(1);
    	pregunta1.setNombrePregunta("Que tal?");
    	pregunta1.setTipoPregunta("Abierta");
    	liPreguntas.add(pregunta1);
    	
    	Respuesta respuesta = new Respuesta();
    	respuesta.setId(1);
    	respuesta.setNombreRespuesta("Bueno");
    	
    	List<Respuesta> liRespuestaRes = new ArrayList();
    	liRespuestaRes.add(respuesta);
    	
    	Pregunta pregunta2 = new Pregunta();
    	pregunta2.setId(2);
    	pregunta2.setNombrePregunta("Que fue?");
    	pregunta2.setTipoPregunta("Multiple");
    	pregunta2.setRespuestas(liRespuestaRes);
    	liPreguntas.add(pregunta2);
    	PreguntaResponseDto preguntaResponseDto = new PreguntaResponseDto();
        preguntaResponseDto.setPreguntas(liPreguntas);
        
    	when(encuestaService.listarRespuestas()).thenReturn(preguntaResponseDto);
    	
    	ResponseEntity response = encuestaController.encuestasConsul();
    	
    	assertThat(response.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    void encuestaCreate() {
    	List liPreguntas = new ArrayList();
        Pregunta pregunta = new Pregunta();
        pregunta.setId(1);
        pregunta.setNombrePregunta("Que tal?");
        pregunta.setTipoPregunta("Abierta");

        liPreguntas.add(pregunta);
        List liRespuestas = new ArrayList();
        liRespuestas.add("1");


        Usuario usuario = new Usuario();
        usuario.setId(1);
        usuario.setNombre("prueba test");
        usuario.setCorreo("prueba@gmail.com");
        usuario.setTelefono("21232343");

        EncuestaRequest encuestaRequest = new EncuestaRequest();
        encuestaRequest.setPreguntas(liPreguntas);
        encuestaRequest.setRespuestas(liRespuestas);
        encuestaRequest.setUsuario(usuario);
    	
    	EncuestaResponse encuestaResponse = new EncuestaResponse();
        encuestaResponse.setMensaje("exito");
    	
    	when(encuestaService.grabarEncuesta(encuestaRequest)).thenReturn(encuestaResponse);
    	
    	ResponseEntity response = encuestaController.encuestaCreate(encuestaRequest);
    	
    	assertThat(response.getStatusCodeValue()).isEqualTo(200);
    }
}