package com.example.encuesta.service.impl;

import com.example.encuesta.dto.EncuestaRequest;
import com.example.encuesta.dto.EncuestaResponse;
import com.example.encuesta.dto.Pregunta;
import com.example.encuesta.dto.PreguntaResponseDto;
import com.example.encuesta.dto.Respuesta;
import com.example.encuesta.dto.Usuario;
import com.example.encuesta.entity.EncuestaEntity;
import com.example.encuesta.entity.PreguntaEntity;
import com.example.encuesta.entity.PreguntaRespuestaEntity;
import com.example.encuesta.entity.RespuestaEntity;
import com.example.encuesta.entity.UsuarioEntity;
import com.example.encuesta.repository.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

class EncuestaServiceTest {

    @InjectMocks
    EncuestaService encuestaService;

    @Mock
    EncuestaRepository encuestaRespository;

    @Mock
    UsuarioRepository usuarioRepository;

    @Mock
    PreguntaRepository preguntaRepository;

    @Mock
    PreguntaRespuestaRepository preguntaRespuestaRepository;

    @Mock
    RespuestaRepository respuestaRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void listarRespuestas() {
    	
    	List<PreguntaEntity> lisPregustaAbiertas = new ArrayList();
    	PreguntaEntity preguntaEntity = new PreguntaEntity();
    	preguntaEntity.setId(1);
    	preguntaEntity.setNombrePregunta("Que tal?");
    	preguntaEntity.setTipoPregunta("Abierta");
    	lisPregustaAbiertas.add(preguntaEntity);
    	
    	List<PreguntaEntity> lisPregustaMultiple = new ArrayList();
    	PreguntaEntity preguntaEntity1 = new PreguntaEntity();
    	preguntaEntity1.setId(2);
    	preguntaEntity1.setNombrePregunta("Que fue?");
    	preguntaEntity1.setTipoPregunta("Multiple");
    	lisPregustaMultiple.add(preguntaEntity1);
    	
    	PreguntaRespuestaEntity preguntaRespuestaEntity = new PreguntaRespuestaEntity();
    	preguntaRespuestaEntity.setId(1);
    	preguntaRespuestaEntity.setIdPregunta(2);
    	preguntaRespuestaEntity.setIdRespuesta(1);
    	
    	List<PreguntaRespuestaEntity> liPreguntaRespuesta = new ArrayList();
    	liPreguntaRespuesta.add(preguntaRespuestaEntity);
    	
    	RespuestaEntity respuestaEntity = new RespuestaEntity();
    	respuestaEntity.setIdRespuesta(1);
    	respuestaEntity.setNombreRespuesta("Bueno");
    	    	

    	when(preguntaRepository.findByTipoPregunta("Abierta")).thenReturn(lisPregustaAbiertas);
    	when(preguntaRepository.findByTipoPregunta("Multiple")).thenReturn(lisPregustaMultiple);
    	when(preguntaRespuestaRepository.findByIdPregunta(2)).thenReturn(liPreguntaRespuesta);
    	when(respuestaRepository.findByIdRespuesta(1)).thenReturn(respuestaEntity);
    	
    	List<Pregunta> liPreguntas = new ArrayList<Pregunta>();
    	Pregunta pregunta1 = new Pregunta();
    	pregunta1.setId(1);
    	pregunta1.setNombrePregunta("Que tal?");
    	pregunta1.setTipoPregunta("Abierta");
    	liPreguntas.add(pregunta1);
    	
    	Respuesta respuesta = new Respuesta();
    	respuesta.setId(1);
    	respuesta.setNombreRespuesta("Bueno");
    	
    	List<Respuesta> liRespuestaRes = new ArrayList();
    	liRespuestaRes.add(respuesta);
    	
    	Pregunta pregunta2 = new Pregunta();
    	pregunta2.setId(2);
    	pregunta2.setNombrePregunta("Que fue?");
    	pregunta2.setTipoPregunta("Multiple");
    	pregunta2.setRespuestas(liRespuestaRes);
    	liPreguntas.add(pregunta2);
    	PreguntaResponseDto preguntaResponseDto = new PreguntaResponseDto();
        preguntaResponseDto.setPreguntas(liPreguntas);
        
        assertEquals(preguntaResponseDto, encuestaService.listarRespuestas());
    }

    @Test
    void grabarEncuesta() {
    	List liPreguntas = new ArrayList();
        Pregunta pregunta = new Pregunta();
        pregunta.setId(1);
        pregunta.setNombrePregunta("Que tal?");
        pregunta.setTipoPregunta("Abierta");

        liPreguntas.add(pregunta);
        List liRespuestas = new ArrayList();
        liRespuestas.add("1");


        Usuario usuario = new Usuario();
        usuario.setId(1);
        usuario.setNombre("prueba test");
        usuario.setCorreo("prueba@gmail.com");
        usuario.setTelefono("21232343");

        EncuestaRequest encuestaRequest = new EncuestaRequest();
        encuestaRequest.setPreguntas(liPreguntas);
        encuestaRequest.setRespuestas(liRespuestas);
        encuestaRequest.setUsuario(usuario);

        UsuarioEntity usuarioEntity = new UsuarioEntity();

        EncuestaEntity encuestaEntity = new EncuestaEntity();


        when(usuarioRepository.save(usuarioEntity)).thenReturn(usuarioEntity);
        when(encuestaRespository.save(encuestaEntity)).thenReturn(encuestaEntity);

        EncuestaResponse encuestaResponse = new EncuestaResponse();
        encuestaResponse.setMensaje("exito");
        assertEquals(encuestaResponse, encuestaService.grabarEncuesta(encuestaRequest));
    }
    
    @Test
    public void dadoPreguntasVacionGrabarEncuestaTest(){
        List liPreguntas = new ArrayList();        
        List liRespuestas = new ArrayList();
        
        Usuario usuario = new Usuario();
        usuario.setId(1);
        usuario.setNombre("prueba test");
        usuario.setCorreo("prueba@gmail.com");
        usuario.setTelefono("21232343");

        EncuestaRequest encuestaRequest = new EncuestaRequest();
        encuestaRequest.setPreguntas(liPreguntas);
        encuestaRequest.setRespuestas(liRespuestas);
        encuestaRequest.setUsuario(usuario);

        UsuarioEntity usuarioEntity = new UsuarioEntity();

        EncuestaEntity encuestaEntity = new EncuestaEntity();


        when(usuarioRepository.save(usuarioEntity)).thenReturn(usuarioEntity);
        
        EncuestaResponse encuestaResponse = new EncuestaResponse();
        encuestaResponse.setMensaje("No hay respuestas que guardar");
        assertEquals(encuestaResponse, encuestaService.grabarEncuesta(encuestaRequest));
    }
}