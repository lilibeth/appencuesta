insert into pregunta_entity (id,nombre_pregunta, tipo_pregunta)values(1, 'Que tal el servicio?', 'Multiple');
insert into pregunta_entity (id,nombre_pregunta, tipo_pregunta)values(2, 'Danos tu opinion?', 'Abierta');
insert into pregunta_entity (id,nombre_pregunta, tipo_pregunta)values(3, 'Que tal la comida?', 'Multiple');

insert into respuesta_entity (id_respuesta,nombre_respuesta)values(1, 'Bueno');
insert into respuesta_entity (id_respuesta,nombre_respuesta)values(2, 'Malo');
insert into respuesta_entity (id_respuesta,nombre_respuesta)values(3, 'Excelente');

insert into pregunta_respuesta_entity (id, id_pregunta,id_respuesta)values(1,1, 1);
insert into pregunta_respuesta_entity (id, id_pregunta,id_respuesta)values(2,1, 2);
insert into pregunta_respuesta_entity (id, id_pregunta,id_respuesta)values(3, 1, 3);
insert into pregunta_respuesta_entity (id, id_pregunta,id_respuesta)values(4,3, 1);
insert into pregunta_respuesta_entity (id, id_pregunta,id_respuesta)values(5, 3, 2);
insert into pregunta_respuesta_entity (id, id_pregunta,id_respuesta)values(6, 3, 3);