package com.example.encuesta.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class RespuestaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idRespuesta;
    private String nombreRespuesta;
}
