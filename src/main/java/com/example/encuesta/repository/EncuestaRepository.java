package com.example.encuesta.repository;

import com.example.encuesta.entity.EncuestaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EncuestaRepository extends JpaRepository<EncuestaEntity, Long> {
}
