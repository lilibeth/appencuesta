package com.example.encuesta.repository;


import com.example.encuesta.entity.PreguntaRespuestaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PreguntaRespuestaRepository  extends JpaRepository<PreguntaRespuestaEntity, Long> {
    public List<PreguntaRespuestaEntity> findByIdPregunta(@Param("idPregunta") int idPregunta );
}
