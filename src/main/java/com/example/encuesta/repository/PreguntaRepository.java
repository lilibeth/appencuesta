package com.example.encuesta.repository;

import com.example.encuesta.entity.PreguntaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PreguntaRepository extends JpaRepository<PreguntaEntity, Long> {
    public List<PreguntaEntity> findByTipoPregunta(@Param("tipoPregunta") String tipoPregunta );
}
