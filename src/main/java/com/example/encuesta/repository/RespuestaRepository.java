package com.example.encuesta.repository;



import com.example.encuesta.entity.RespuestaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RespuestaRepository extends JpaRepository<RespuestaEntity, Long> {

    public RespuestaEntity findByIdRespuesta(@Param("idRespuesta") int idRespuesta );
}
