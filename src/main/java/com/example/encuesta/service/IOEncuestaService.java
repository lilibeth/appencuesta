package com.example.encuesta.service;

import com.example.encuesta.dto.EncuestaRequest;
import com.example.encuesta.dto.EncuestaResponse;
import com.example.encuesta.dto.PreguntaResponseDto;

public interface IOEncuestaService {
    public PreguntaResponseDto listarRespuestas();
    public EncuestaResponse grabarEncuesta(EncuestaRequest encuestaRequest);
}
