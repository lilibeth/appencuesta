package com.example.encuesta.service.impl;

import com.example.encuesta.dto.*;
import com.example.encuesta.entity.*;
import com.example.encuesta.repository.*;
import com.example.encuesta.service.IOEncuestaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class EncuestaService  implements IOEncuestaService {

    @Autowired
    EncuestaRepository encuestaRespository;

    @Autowired
    UsuarioRepository usuarioRepository;

    @Autowired
    PreguntaRepository preguntaRepository;

    @Autowired
    PreguntaRespuestaRepository preguntaRespuestaRepository;

    @Autowired
    RespuestaRepository respuestaRepository;


    @Override
    public PreguntaResponseDto listarRespuestas() {

        List liPreguntas = new ArrayList();
        for (PreguntaEntity preguntaEntity : preguntaRepository.findByTipoPregunta("Abierta"))
        {
            Pregunta pregunta = new Pregunta();
            pregunta.setId(preguntaEntity.getId());
            pregunta.setNombrePregunta(preguntaEntity.getNombrePregunta());
            pregunta.setTipoPregunta(preguntaEntity.getTipoPregunta());
            liPreguntas.add(pregunta);
        }

        for (PreguntaEntity preguntaEntity : preguntaRepository.findByTipoPregunta("Multiple")){
            Pregunta pregunta = new Pregunta();
            pregunta.setId(preguntaEntity.getId());
            pregunta.setNombrePregunta(preguntaEntity.getNombrePregunta());
            pregunta.setTipoPregunta(preguntaEntity.getTipoPregunta());
            List<Respuesta> lirespuestas = new ArrayList();
            List<PreguntaRespuestaEntity> liPregustaespuesta = preguntaRespuestaRepository.findByIdPregunta(preguntaEntity.getId());
            if(!liPregustaespuesta.isEmpty()){
                for(PreguntaRespuestaEntity preguntaRespuestaEntity: liPregustaespuesta){
                    RespuestaEntity respuestaEntity = respuestaRepository.findByIdRespuesta(
                                               preguntaRespuestaEntity.getIdRespuesta()
                                               );

                    Respuesta respuesta = new Respuesta();
                    if(respuestaEntity != null){
                        respuesta.setId(respuestaEntity.getIdRespuesta());
                        respuesta.setNombreRespuesta(respuestaEntity.getNombreRespuesta());
                    }
                    lirespuestas.add(respuesta);
                }
            }
            pregunta.setRespuestas(lirespuestas);
            liPreguntas.add(pregunta);
        }
        PreguntaResponseDto preguntaResponseDto = new PreguntaResponseDto();
        preguntaResponseDto.setPreguntas(liPreguntas);
        return preguntaResponseDto;
    }

    @Override
    public EncuestaResponse grabarEncuesta(EncuestaRequest encuestaRequest) {
        String mensaje = "";
        UsuarioEntity usuarioEntity = new UsuarioEntity();
        usuarioEntity.setId(encuestaRequest.getUsuario().getId());
        usuarioEntity.setNombre(encuestaRequest.getUsuario().getNombre());
        usuarioEntity.setCorreo(encuestaRequest.getUsuario().getCorreo());
        usuarioEntity.setTelefono(encuestaRequest.getUsuario().getTelefono());
        usuarioRepository.save(usuarioEntity);
        int cont = 0;
        Date fechaRespuesta = new Date();
        List listaRespuesta = encuestaRequest.getRespuestas();
        if(!encuestaRequest.getPreguntas().isEmpty()){
            for (Pregunta pregunta : encuestaRequest.getPreguntas())
            {
                String respuesta = listaRespuesta.get(cont).toString();
                EncuestaEntity encuestaEntity = new EncuestaEntity();
                encuestaEntity.setId(pregunta.getId());
                encuestaEntity.setIdUsuario(encuestaRequest.getUsuario().getId());
                encuestaEntity.setIdPregunta(pregunta.getId());
                encuestaEntity.setFecha(fechaRespuesta);
                encuestaEntity.setRespuesta(respuesta);
                encuestaRespository.save(encuestaEntity);
                cont += 1;
            }
            mensaje = "exito";
        } else {
            mensaje = "No hay respuestas que guardar";
        }

        EncuestaResponse encuestaResponse = new EncuestaResponse();
        encuestaResponse.setMensaje(mensaje);
        return encuestaResponse;
    }
}
