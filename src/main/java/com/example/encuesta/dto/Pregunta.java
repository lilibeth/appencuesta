package com.example.encuesta.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Pregunta {

    @JsonProperty("id")
    private int id;
    @JsonProperty("nombrePregunta")
    private String nombrePregunta;
    @JsonProperty("tipoPregunta")
    private String tipoPregunta;
    private List<Respuesta> respuestas;
}
