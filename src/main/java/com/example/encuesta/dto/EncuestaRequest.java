package com.example.encuesta.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EncuestaRequest {

    @JsonProperty("usuario")
    private Usuario usuario;
    @JsonProperty("preguntas")
    private List<Pregunta> preguntas;
    @JsonProperty("respuestas")
    private List<String> respuestas;
}
