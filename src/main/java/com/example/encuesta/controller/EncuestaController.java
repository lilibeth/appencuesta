package com.example.encuesta.controller;

import com.example.encuesta.dto.EncuestaRequest;
import com.example.encuesta.dto.EncuestaResponse;
import com.example.encuesta.dto.PreguntaResponseDto;
import com.example.encuesta.service.IOEncuestaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;

@RestController
public class EncuestaController implements Serializable {

    @Autowired
    IOEncuestaService ioEncuestaService;

    @GetMapping(value = "/encuestas/", consumes = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<PreguntaResponseDto> encuestasConsul() {
        try {

            return ResponseEntity.status(HttpStatus.OK).body(ioEncuestaService.listarRespuestas());

        } catch (Exception e) {
            return ResponseEntity.notFound().build();

        }
    }

    @PostMapping(value = "/responderencuenta/", consumes = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<EncuestaResponse> encuestaCreate(@RequestBody EncuestaRequest encuestaRequest) {

        try {

            return ResponseEntity.status(HttpStatus.OK).body(ioEncuestaService.grabarEncuesta(encuestaRequest));

        } catch (Exception e) {
            return ResponseEntity.notFound().build();

        }

    }
}
